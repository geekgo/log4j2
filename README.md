Log4j 2 包含了基于LMAX 分离库的下一代的异步日志系统，在多线程环境下，异步日志系统比 Log4j 1.x 和Logback 提高了10倍性能提升（吞吐量和延迟率 ）。原文如下:
Log4j 2 contains next-generation Asynchronous Loggers based on the LMAX Disruptor library. In multi-threaded scenarios Asynchronous Loggers have 10 times higher throughput and orders of magnitude lower latency than Log4j 1.x and Logback. 

由于作者推荐，为了性能考虑，尽量不要使用slf4j，但我依赖的第三方框架比如spring有对slf4j使用，所以对JAR包做了以上的取舍，所以原有代码中要做如下的改动
改动前:import org.slf4j.Logger;
      import org.slf4j.LoggerFactory;
     private static final Logger logger = LoggerFactory.getLogger(OOXX.class);

改动后:iimport org.apache.logging.log4j.LogManager;
      import org.apache.logging.log4j.Logger;
     private static final Logger logger = LogManager.getLogger(OOXX.class);

要使用异步写日志的功能，必须引入Disruptor
<asyncRoot> or <asyncLogger>
官方建议一般程序员查看的日志改成异步方式，一些运营日志改成同步

Asynchronous Appenders 和 Asynchronous Loggers 区别:
在</appenders> 节点里添加
  <Async name="Async">
      <AppenderRef ref="MyFile"/>
    </Async>
为Asynchronous Appenders 性能比同步快，比Asynchronous Loggers慢
在loggers节点添加
    <AsyncLogger name="com.foo.Bar" level="trace" includeLocation="true">
      <AppenderRef ref="RandomAccessFile"/>
    </AsyncLogger>
或者添加
<!-- Root Logger -->
<asyncRoot level="DEBUG">
<appender-ref ref="DevLog" />
<appender-ref ref="Console" />
</asyncRoot>
为logger async 用的是无锁并发技术，必须引入Disruptor
测试了下，单线程异步比同步效率提高了1倍。线程越多性能提高越明显。
如果要加上位置信息比如哪个类，第几行，需要设置 includeLocation="true"
 但默认不设置好像也是true,