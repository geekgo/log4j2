package com.geekgo.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HeHaoyang
 * @version 2016/6/21 10:06
 */
@RestController
public class LogController {

    private static final Logger log = LogManager.getLogger(LogController.class);

    @RequestMapping("/test")
    public String log(){
        log.trace("log start。。。");
        if (log.isDebugEnabled()){
            log.debug("log debug");
        }
        return "log";
    }
}
